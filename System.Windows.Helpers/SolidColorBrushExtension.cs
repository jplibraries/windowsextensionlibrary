﻿using System.Windows.Media;

namespace System.Windows.Helpers
{
    public static class SolidColorBrushExtension
    {
        public static SolidColorBrush Brightness(this SolidColorBrush color, float brightnessFactor = 5.0f)
        {
            return new SolidColorBrush(color.Color.Brightness(brightnessFactor));
        }

        public static SolidColorBrush Darker(this SolidColorBrush color, float correctionFactor = 50f)
        {
            return new SolidColorBrush(color.Color.Darker(correctionFactor));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        /// <param name="opacity">The value of the System.Windows.Media.Brush.Opacity property is expressed as  a value between 0.0 and 1.0. The default value is 1.0.</param>
        /// <returns></returns>
        public static SolidColorBrush ChangeOpacity(this SolidColorBrush color, double opacity = 1.0f)
        {
            color.Opacity = opacity;
            return color;
        }

        /// <summary>
        /// Creates SolidColorBrush with corrected brightness.
        /// </summary>
        /// <param name="color">Color to correct.</param>
        /// <param name="correctionFactor">The brightness correction factor. Must be between -1 and 1.
        /// Negative values produce darker colors.</param>
        /// <returns>Corrected <see cref="Color"/> structure.</returns>
        public static SolidColorBrush ChangeLightness(this SolidColorBrush color, float correctionFactor = 0f)
        {
            return new SolidColorBrush(color.Color.ChangeLightness(correctionFactor));
        }
    }
}
