﻿using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Helpers.AttachedProperies;
using System.Windows.Interop;

namespace System.Windows.Helpers
{
    /// <summary>
    /// Snap layou menu for Windows 11 and <see cref="WindowStyle.None"/> width custom Maximize/Restore button or checkbox.
    /// </summary>
    public class SnapLayoutMenu
    {
        private const int WM_NCHITTEST = 0x0084;//InteropValues
        private const int WM_NCLBUTTONDOWN = 0x00A1;
        private const int WM_NCLBUTTONUP = 0x00A2;
        private const int HTMAXBUTTON = 9;

        /// <summary>
        /// The window handle.
        /// </summary>
        private readonly Window _window;

        /// <summary>
        /// The button to handle mouse over and press.
        /// Might be Button or CheckBox.
        /// </summary>
        private readonly ButtonBase _btnMaximizeRestore;

        /// <summary>
        /// Constructor for Windows 11 Snap layout menu.
        /// </summary>
        /// <param name="window">Windows handler.</param>
        /// <param name="btnMaximizeRestore">Custom Maximize/restore Button or CheckBox object, must implement atteched property <see cref="SnapButtonHelper"/>.</param>
        /// <exception cref="NullReferenceException"></exception>
        public SnapLayoutMenu(Window window, ButtonBase btnMaximizeRestore)
        {
            _window = window ?? throw new NullReferenceException(nameof(window));
            _btnMaximizeRestore = btnMaximizeRestore ?? throw new NullReferenceException(nameof(btnMaximizeRestore));

            // Listen out for source initialized to setup
            _window.SourceInitialized += Window_SourceInitialized;
        }

        /// <summary>
        /// Constructor for Windows 11 Snap layout menu.
        /// </summary>
        /// <param name="window">Windows handler.</param>
        /// <param name="buttonName">Custom Maximize/restore Button or CheckBox name, must implement atteched property <see cref="SnapButtonHelper"/>.</param>
        /// <exception cref="NullReferenceException"></exception>
        public SnapLayoutMenu(Window window, string buttonName)
        {
            _window = window ?? throw new NullReferenceException(nameof(window));
            _btnMaximizeRestore = (ButtonBase)window.FindName(buttonName) ?? throw new NullReferenceException(buttonName);

            // Listen out for source initialized to setup
            _window.SourceInitialized += Window_SourceInitialized;
        }

        /// <summary>
        /// Initialize and hook into the windows message pump
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_SourceInitialized(object sender, EventArgs e)
        {
            IntPtr hwnd = new WindowInteropHelper(_window).Handle;
            HwndSource.FromHwnd(hwnd).AddHook(new HwndSourceHook(WndProc));
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            //https://learn.microsoft.com/en-us/windows/apps/desktop/modernize/apply-snap-layout-menu
            //https://github.com/dotnet/wpf/issues/4825
            switch (msg)
            {
                case WM_NCHITTEST:
                    try
                    {
                        if (OSVersionHelper.IsWindows11_OrGreater && _window.WindowStyle == WindowStyle.None)
                        {
                            // Pozycja kursora w obszarze klienta
                            Point pos = _window.PointFromScreen(new Point((int)lParam & 0xFFFF, (int)lParam >> 16));

                            // Sprawdź, czy kursor znajduje się nad przyciskiem maksymalizacji
                            if (IsCursorOnMaximizeButton(pos))
                            {
                                handled = true;
                                SnapButtonHelper.SetIsMouseOverButtonWidthSnap(_btnMaximizeRestore, true);
                                return new IntPtr(HTMAXBUTTON);
                            }
                            else
                            {
                                SnapButtonHelper.SetIsMouseOverButtonWidthSnap(_btnMaximizeRestore, false);
                                SnapButtonHelper.SetIsPressedButtonWidthSnap(_btnMaximizeRestore, false);
                            }
                        }
                    }
                    catch (OverflowException)
                    {
                        handled = true;
                    }
                    break;
                case WM_NCLBUTTONDOWN:
                    if (OSVersionHelper.IsWindows11_OrGreater && _window.WindowStyle == WindowStyle.None)
                    {
                        // Pozycja kursora w obszarze klienta
                        Point clickPos = _window.PointFromScreen(new Point((int)lParam & 0xFFFF, (int)lParam >> 16));

                        // Sprawdź, czy kursor znajduje się nad przyciskiem maksymalizacji
                        if (IsCursorOnMaximizeButton(clickPos))
                        {
                            handled = true;
                            SnapButtonHelper.SetIsPressedButtonWidthSnap(_btnMaximizeRestore, true);
                        }
                    }
                    break;
                case WM_NCLBUTTONUP:
                    if (OSVersionHelper.IsWindows11_OrGreater && _window.WindowStyle == WindowStyle.None)
                    {
                        // Pozycja kursora w obszarze klienta
                        Point clickPos = _window.PointFromScreen(new Point((int)lParam & 0xFFFF, (int)lParam >> 16));

                        // Sprawdź, czy kursor znajduje się nad przyciskiem maksymalizacji
                        if (IsCursorOnMaximizeButton(clickPos))
                        {
                            handled = true;
                            SnapButtonHelper.SetIsPressedButtonWidthSnap(_btnMaximizeRestore, false);
                            if (_btnMaximizeRestore is Button btnMax)
                            {
                                IInvokeProvider invokeProv = new ButtonAutomationPeer(btnMax).GetPattern(PatternInterface.Invoke) as IInvokeProvider;
                                invokeProv?.Invoke();
                            }
                            else if (_btnMaximizeRestore is CheckBox cbMax)
                            {
                                ToggleButtonAutomationPeer checkBoxPeer = new ToggleButtonAutomationPeer(cbMax as CheckBox);
                                IToggleProvider toggleProv = checkBoxPeer.GetPattern(PatternInterface.Toggle) as IToggleProvider;
                                toggleProv?.Toggle();

                                _window.WindowState = (bool)cbMax.IsChecked ? WindowState.Maximized : WindowState.Normal;
                            }
                        }
                    }
                    break;
                default:
                    handled = false;
                    break;
            }

            return IntPtr.Zero;
        }

        private bool IsCursorOnMaximizeButton(Point cursor)
        {
            if (_btnMaximizeRestore is null)
            {
                return false;
            }

            var btn = _btnMaximizeRestore;

            Point relativePoint = btn.TransformToAncestor(_window).Transform(new Point(0, 0));
            Rect btnBounds = new Rect(relativePoint, new Size(btn.ActualWidth, btn.ActualHeight));

            return btnBounds.Contains(cursor);
        }

    }
}
