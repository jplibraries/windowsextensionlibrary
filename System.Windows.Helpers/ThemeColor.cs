﻿using System.Windows.Media;

namespace System.Windows.Helpers
{
    /// <summary>
    /// The leading color of Windows.
    /// </summary>
    public class ThemeColor
    {
        public static Color AccentColor
        {
            get
            {
                if (Environment.OSVersion.Version.Major >= 6 && CurrentVersion.CurrentBuildNumber > (uint)CurrentVersion.BuildNumbers.Win10_Threshold1)
                    return AccentColorSet.ActiveSet["SystemAccent"];
                else return RandomAccesColor.RandomColor;
            }
        }

        public static SolidColorBrush AccentBrush
        {
            get
            {
                if (Environment.OSVersion.Version.Major >= 6 && CurrentVersion.CurrentBuildNumber > (uint)CurrentVersion.BuildNumbers.Win10_Threshold1)
                    return new SolidColorBrush(AccentColorSet.ActiveSet["SystemAccent"]);
                else return new SolidColorBrush(RandomAccesColor.RandomColor);
            }
        }
    }
}
