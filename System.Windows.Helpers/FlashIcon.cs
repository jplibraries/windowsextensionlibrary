﻿using System.Runtime.InteropServices;
using System.Windows.Interop;

namespace System.Windows
{
    /// <summary>
    /// Provides functionality to flash a window icon on the taskbar.
    /// </summary>
    public static class FlashIcon
    {
        /// <summary>
        /// Flashes the specified window. This function sends a message to the specified window to invert the state of the window icon on the taskbar.
        /// </summary>
        /// <param name="hwnd">A handle to the window to be flashed.</param>
        /// <param name="bInvert">If this parameter is TRUE, the window is flashed from one state to the other. If it is FALSE, the window is returned to its original state (either active or inactive).</param>
        /// <returns>If the window was active before the call, the return value is nonzero. Otherwise, the return value is zero.</returns>
        [DllImport("user32")] public static extern int FlashWindow(IntPtr hwnd, bool bInvert);

        /// <summary>
        /// Blinks the window on the taskbar.
        /// </summary>
        /// <param name="win">The window to flash.</param>
        /// <param name="invert">Determines if the window icon's state should be inverted. Defaults to true.</param>
        public static void Flash(this Window win, bool invert = true)
        {
            WindowInteropHelper wih = new WindowInteropHelper(win);
            FlashWindow(wih.Handle, invert);
        }
    }
}
