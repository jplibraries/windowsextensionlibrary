﻿using System.Windows.Media;

namespace System.Windows.Helpers
{
    public class RandomAccesColor
    {
        private static readonly string[] AccentColorPack = { "#FFA4C400", "#FF60A917", "#FF008A00", "#FF00ABA9", "#FF1BA1E2", "#FF0050EF",
        "#FF6A00FF", "#FFAA00FF", "#FFF472D0", "#FFD80073", "#FFA20025", "#FFE51400", "#FFFA6800", "#FFF0A30A",
        "#FFE3C800", "#FF825A2C", "#FF6D8764", "#FF647687", "#FF76608A", "#FFA0522D", "#FF87794E"};
        private readonly static int randomValue = new Random().Next(0, AccentColorPack.Length - 1);

        public static Color RandomColor => (Color)ColorConverter.ConvertFromString(AccentColorPack[randomValue]);
    }
}
