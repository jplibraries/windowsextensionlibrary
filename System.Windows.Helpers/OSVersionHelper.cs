﻿using static System.Windows.CurrentVersion;

namespace System.Windows.Helpers
{
    /// <summary>
    /// Class for fast determine Windows Version.
    /// </summary>
    public static class OSVersionHelper
    {
        /// <summary>
        /// Windows 7 Or Greater
        /// </summary>
        public static bool IsWindows7_OrGreater { get; } = CurrentBuildNumber >= (uint)BuildNumbers.Win7;

        /// <summary>
        /// Windows 8 Or Greater
        /// </summary>
        public static bool IsWindows8_OrGreater { get; } = CurrentBuildNumber >= (uint)BuildNumbers.Win8;

        /// <summary>
        /// Windows 10 Or Greater
        /// </summary>
        public static bool IsWindows10_OrGreater { get; } = CurrentBuildNumber >= (uint)BuildNumbers.Win10_19H1;

        /// <summary>
        /// Windows 11 Or Greater
        /// </summary>
        public static bool IsWindows11_OrGreater { get; } = CurrentBuildNumber >= (uint)BuildNumbers.Win11_21H2;

    }
}
