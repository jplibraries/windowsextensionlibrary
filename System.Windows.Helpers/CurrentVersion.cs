﻿using Microsoft.Win32;

namespace System.Windows
{
    /// <summary>
    /// Provide information on Windows version.
    /// </summary>
    public static class CurrentVersion
    {
        /// <summary>
        /// Enumerates the build numbers of various Windows versions.
        /// </summary>
        public enum BuildNumbers
        {
            /// <summary>Windows 95</summary>
            Win95 = 950,
            /// <summary>Windows 95A</summary>
            Win95A = 1111,
            /// <summary>Windows 95B (OSR2)</summary>
            Win95B = 1214,
            /// <summary>Windows 95C (OSR2.5)</summary>
            Win95C = 1214,

            /// <summary>Windows 98</summary>
            Win98 = 1998,
            /// <summary>Windows 98 Second Edition</summary>
            Win98SE = 2222,

            /// <summary>Windows Millennium Edition</summary>
            WinME = 3000,

            /// <summary>Windows NT 3.51</summary>
            WinNT351 = 1057,
            /// <summary>Windows NT 4.0</summary>
            WinNT40 = 1381,

            /// <summary>Windows 2000</summary>
            Win2000 = 2195,

            /// <summary>Windows XP</summary>
            WinXP = 2600,
            /// <summary>Windows XP SP2</summary>
            WinXPSP2 = 2180,
            /// <summary>Windows XP SP3</summary>
            WinXPSP3 = 5512,

            /// <summary>Windows Vista</summary>
            WinVista = 6000,
            /// <summary>Windows Vista SP1</summary>
            WinVistaSP1 = 6001,
            /// <summary>Windows Vista SP2</summary>
            WinVistaSP2 = 6002,

            /// <summary>Windows 7</summary>
            Win7 = 7600,
            /// <summary>Windows 7 SP1</summary>
            Win7SP1 = 7601,

            /// <summary>Windows 8</summary>
            Win8 = 9200,
            /// <summary>Windows 8.1</summary>
            Win81 = 9600,
            /// <summary>Windows 8.1 Update 1</summary>
            Win81Update1 = 9600,

            /// <summary>Windows 10 (Threshold 1)</summary>
            Win10_Threshold1 = 10240,
            /// <summary>Windows 10 November Update (Threshold 2)</summary>
            Win10_Threshold2 = 10586,
            /// <summary>Windows 10 Anniversary Update (Redstone 1)</summary>
            Win10_Redstone1 = 14393,
            /// <summary>Windows 10 Creators Update (Redstone 2)</summary>
            Win10_Redstone2 = 15063,
            /// <summary>Windows 10 Fall Creators Update (Redstone 3)</summary>
            Win10_Redstone3 = 16299,
            /// <summary>Windows 10 April 2018 Update (Redstone 4)</summary>
            Win10_Redstone4 = 17134,
            /// <summary>Windows 10 October 2018 Update (Redstone 5)</summary>
            Win10_Redstone5 = 17763,
            /// <summary>Windows 10 May 2019 Update (19H1)</summary>
            Win10_19H1 = 18362,
            /// <summary>Windows 10 November 2019 Update (19H2)</summary>
            Win10_19H2 = 18363,
            /// <summary>Windows 10 May 2020 Update (20H1)</summary>
            Win10_20H1 = 19041,
            /// <summary>Windows 10 October 2020 Update (20H2)</summary>
            Win10_20H2 = 19042,
            /// <summary>Windows 10 May 2021 Update (21H1)</summary>
            Win10_21H1 = 19043,
            /// <summary>Windows 10 November 2021 Update (21H2)</summary>
            Win10_21H2 = 19044,

            /// <summary>Windows 11 Original release (21H2)</summary>
            Win11_21H2 = 22000,
            /// <summary>Windows 11 2022 Update (22H2)</summary>
            Win11_22H2 = 22621,
            /// <summary>Windows 11 2023 Update (23H2)</summary>
            Win11_23H2 = 25915
        }

        /// <summary>
        /// Gets the release ID of the current Windows version. Only Windows 10 and later return a value for the release.
        /// </summary>
        public static uint ReleaseId
        {
            get
            {
                uint.TryParse(Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ReleaseId", "").ToString(), out uint number);
                return number;
            }
        }

        /// <summary>
        /// Gets the product name of the current Windows version.
        /// </summary>
        public static string ProductName
        {
            get
            {
                return Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName", "").ToString();
            }
        }

        /// <summary>
        /// Gets the display version (codename) of the current Windows version.
        /// </summary>
        public static string DisplayVersion
        {
            get
            {
                return Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "DisplayVersion", "").ToString();
            }
        }

        /// <summary>
        /// Gets the current build number of the Windows version.
        /// </summary>
        public static uint CurrentBuildNumber
        {
            get
            {
                uint.TryParse(Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "CurrentBuildNumber", "").ToString(), out uint number);
                return number;
            }
        }
    }
}
