﻿using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Screen;
using WinForms = System.Windows.Forms;

namespace System.Windows.Helpers
{
    /// <summary>
    /// Extension Windows class to get Screen inforamtion.
    /// </summary>
    public static class WindowsExtension
    {
        /// <summary>
        /// Get screen size
        /// </summary>
        /// <param name="window"></param>
        /// <returns></returns>
        public static ScreenSize CurrentScreenSize(this Window window)
        {
            try
            {
                var dpiScale = VisualTreeHelper.GetDpi(window);

                //var screen = Screen.FromPoint(new System.Drawing.Point((int)window.Left, (int)window.Top));
                var screen = WinForms.Screen.FromHandle(new WindowInteropHelper(window).Handle);

                return new ScreenSize(new System.Windows.Size(screen.Bounds.Width, screen.Bounds.Height),
                    new Rect(screen.WorkingArea.Left / dpiScale.DpiScaleX, screen.WorkingArea.Top / dpiScale.DpiScaleY,
                            screen.WorkingArea.Width / dpiScale.DpiScaleX, screen.WorkingArea.Height / dpiScale.DpiScaleY), dpiScale.DpiScaleY, screen.Primary);
            }
            catch (Exception)
            {
                //dpi = System.WindowsFonts.FontDPI / 100f;//%
                return new ScreenSize(new System.Windows.Size(SystemParameters.PrimaryScreenWidth, SystemParameters.PrimaryScreenHeight), SystemParameters.WorkArea, 1, true);
            }
        }

        /// <summary>
        /// Retrieves the screen that contains the specified window.
        /// </summary>
        /// <param name="window">The window for which to retrieve the screen.</param>
        /// <returns>The screen that contains the specified window.</returns>
        public static WinForms.Screen CurrentScreen(this Window window)
        {
            return WinForms.Screen.FromPoint(new System.Drawing.Point((int)window.Left, (int)window.Top));
        }

        /// <summary>
        /// Get the current mouse position on the screen
        /// </summary>
        /// <param name="window"></param>
        /// <returns></returns>
        public static Point MousePosition(this Window window)
        {
            //Position mouse reletive to the mouse
            var position = Mouse.GetPosition(window);

            //Add windows position to reletive position
            return new Point(position.X + window.Left, position.Y + window.Top);
        }
    }
}
