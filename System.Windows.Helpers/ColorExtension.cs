﻿using System.Windows.Media;

namespace System.Windows.Helpers
{
    public static class ColorExtension
    {
        public static Color Brightness(this Color color, float brightnessFactor = 5.0f)
        {
            brightnessFactor /= 10.0f;

            Color adjustedColor = Color.FromArgb(color.A,
                ClampToByte(color.R * brightnessFactor),
                ClampToByte(color.G * brightnessFactor),
                ClampToByte(color.B * brightnessFactor));
            return adjustedColor;
        }

        public static Color Darker(this Color color, float correctionFactor = 50f)
        {
            const float hundredPercent = 100f;
            correctionFactor /= hundredPercent;

            return Color.FromArgb(color.A,
                ClampToByte(color.R * correctionFactor),
                ClampToByte(color.G * correctionFactor),
                ClampToByte(color.B * correctionFactor));
        }

        /// <summary>
        /// Creates color with corrected brightness.
        /// </summary>
        /// <param name="color">Color to correct.</param>
        /// <param name="coef">The brightness correction factor. Must be between -1 and 1.
        /// Negative values produce darker colors.</param>
        /// <returns>Corrected <see cref="Color"/> structure.</returns>
        public static Color ChangeLightness(this Color color, float coef)
        {
            float red = (float)color.R;
            float green = (float)color.G;
            float blue = (float)color.B;

            if (coef < 0)
            {
                coef = 1 + coef;
                red *= coef;
                green *= coef;
                blue *= coef;
            }
            else
            {
                red = (255 - red) * coef + red;
                green = (255 - green) * coef + green;
                blue = (255 - blue) * coef + blue;
            }

            return Color.FromArgb(color.A, ClampToByte(red), ClampToByte(green), ClampToByte(blue));
        }

        private static byte ClampToByte(float value) => (byte)Clamp(value, 0, 255);

        private static float Clamp(float value, float min, float max)
        {
            if (value < min) return min;
            if (value > max) return max;
            return value;
        }

        /// <summary>
        /// Calculate contrast color.
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static Color ContrastColor(this Color color)
        {
            // Calculate the perceptive luminance (aka luma) - human eye favors green color...
            double luma = ((0.299 * color.R) + (0.587 * color.G) + (0.114 * color.B)) / 255;

            // Return black for bright colors, white for dark colors
            return luma > 0.5 ? Color.FromRgb(0, 0, 0) : Color.FromRgb(225, 255, 255);//Black | White
        }

        /// <summary>
        /// Get HTMl format string color.
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static string ToHtmlString(this Color color) => $"#{color.R:X2}{color.G:X2}{color.B:X2}{color.A:X2}";
    }
}
