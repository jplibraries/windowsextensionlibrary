﻿using Microsoft.Win32;

namespace System.Windows.Helpers
{
    /// <summary>
    /// Specifies the available theme types.
    /// </summary>
    public enum ThemeType
    {
        /// <summary>
        /// Dark theme.
        /// </summary>
        Dark,

        /// <summary>
        /// Light theme.
        /// </summary>
        Light
    }

    /// <summary>
    /// Provides information about the current theme mode settings.
    /// </summary>
    public static class ThemeMode
    {
        private static bool PersonalizeValue(string name, bool defaultValue)
        {
            try
            {
                object res = Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize", name, "");
                return res != null ? res.ToString().Equals("1") : defaultValue;
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }


        /// <summary>
        /// Does windows use a light theme for the application.
        /// </summary>
        public static bool IsAppsUseLightTheme => PersonalizeValue("AppsUseLightTheme", true);

        /// <summary>
        /// Theme type for the application.
        /// </summary>
        public static ThemeType AppsTheme => IsAppsUseLightTheme ? ThemeType.Light : ThemeType.Dark;

        /// <summary>
        /// Does windows use a light theme for the system.
        /// </summary>
        public static bool IsSystemUseLightTheme => PersonalizeValue("SystemUsesLightTheme", true);

        /// <summary>
        /// Theme type for the system.
        /// </summary>
        public static ThemeType SystemTheme => IsSystemUseLightTheme ? ThemeType.Light : ThemeType.Dark;

        /// <summary>
        /// Gets the value indicating the color prevalence setting from the registry.
        /// </summary>
        public static string ColorPrevalence
        {
            get
            {
                object res = Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize", "ColorPrevalence", "");
                if (res != null)
                {
                    return res.ToString();
                }
                return "";
            }
        }

        /// <summary>
        /// Is transparency enabled in window.
        /// </summary>
        public static bool IsTransparencyEnabled => PersonalizeValue("EnableTransparency", false);

    }
}
