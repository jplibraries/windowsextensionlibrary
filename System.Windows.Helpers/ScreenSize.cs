﻿namespace System.Windows.Screen
{
    /// <summary>
    /// Represents the screen size and related properties.
    /// </summary>
    public class ScreenSize
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScreenSize"/> class.
        /// </summary>
        /// <param name="fullSize">The full size of the screen.</param>
        /// <param name="workingArea">The working area of the screen.</param>
        /// <param name="dpi">The DPI (dots per inch) of the screen.</param>
        /// <param name="isPrimary">Indicates whether the screen is the primary screen.</param>
        public ScreenSize(Size fullSize, Rect workingArea, double dpi, bool isPrimary)
        {
            FullSize = fullSize;
            WorkingArea = workingArea;
            Dpi = dpi;
            IsPrimary = isPrimary;
        }

        /// <summary>
        /// Gets the full size of the screen.
        /// </summary>
        public Size FullSize { get; private set; }

        /// <summary>
        /// Gets the working area of the screen.
        /// </summary>
        public Rect WorkingArea { get; private set; }

        /// <summary>
        /// Gets the DPI (dots per inch) of the screen.
        /// </summary>
        public double Dpi { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the screen is the primary screen.
        /// </summary>
        public bool IsPrimary { get; private set; }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return WorkingArea.ToString();
        }
    }
}
