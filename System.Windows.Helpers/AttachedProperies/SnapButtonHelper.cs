﻿using System.Windows;

namespace System.Windows.Helpers.AttachedProperies
{
    /// <summary>
    /// Provides attached properties to handle snap behaviors for button width when the mouse is over or the button is pressed.
    /// </summary>
    public class SnapButtonHelper
    {
        /// <summary>
        /// Identifies the IsMouseOverButtonWidthSnap attached property.
        /// </summary>
        public static readonly DependencyProperty IsMouseOverButtonWidthSnapProperty =
            DependencyProperty.RegisterAttached("IsMouseOverButtonWidthSnap", typeof(bool), typeof(SnapButtonHelper), new PropertyMetadata(default(bool)));

        /// <summary>
        /// Sets the value of the IsMouseOverButtonWidthSnap attached property.
        /// </summary>
        /// <param name="element">The element on which to set the property.</param>
        /// <param name="value">The value to set.</param>
        public static void SetIsMouseOverButtonWidthSnap(DependencyObject element, bool value)
        {
            element.SetValue(IsMouseOverButtonWidthSnapProperty, value);
        }

        /// <summary>
        /// Gets the value of the IsMouseOverButtonWidthSnap attached property.
        /// </summary>
        /// <param name="element">The element from which to get the property.</param>
        /// <returns>The value of the IsMouseOverButtonWidthSnap property.</returns>
        public static bool GetIsMouseOverButtonWidthSnap(DependencyObject element)
        {
            return (bool)element.GetValue(IsMouseOverButtonWidthSnapProperty);
        }


        /// <summary>
        /// Identifies the IsPressedButtonWidthSnap attached property.
        /// </summary>
        public static readonly DependencyProperty IsPressedButtonWidthSnapProperty =
            DependencyProperty.RegisterAttached("IsPressedButtonWidthSnap", typeof(bool), typeof(SnapButtonHelper), new PropertyMetadata());

        /// <summary>
        /// Sets the value of the IsPressedButtonWidthSnap attached property.
        /// </summary>
        /// <param name="element">The element on which to set the property.</param>
        /// <param name="value">The value to set.</param>
        public static void SetIsPressedButtonWidthSnap(DependencyObject element, bool value)
        {
            element.SetValue(IsPressedButtonWidthSnapProperty, value);
        }

        /// <summary>
        /// Gets the value of the IsPressedButtonWidthSnap attached property.
        /// </summary>
        /// <param name="element">The element from which to get the property.</param>
        /// <returns>The value of the IsPressedButtonWidthSnap property.</returns>
        public static bool GetIsPressedButtonWidthSnap(DependencyObject element)
        {
            return (bool)element.GetValue(IsPressedButtonWidthSnapProperty);
        }

    }
}
