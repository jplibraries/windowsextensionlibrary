﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using WinForms = System.Windows.Forms;
using System.Windows.Shapes;
using Point = System.Windows.Point;

namespace System.Windows.Helpers
{
    /// <summary>
    /// Represents a wrapper for accessing screen information in WPF applications.
    /// </summary>
    public class WpfScreen
    {
        private readonly WinForms.Screen screen;

        /// <summary>
        /// Initializes a new instance of the <see cref="WpfScreen"/> class based on a WinForms Screen object.
        /// </summary>
        /// <param name="screen">The WinForms Screen object to wrap.</param>
        internal WpfScreen(WinForms.Screen screen)
        {
            this.screen = screen ?? throw new ArgumentNullException(nameof(screen));
        }

        /// <summary>
        /// Retrieves all screens in the system.
        /// </summary>
        /// <returns>An IEnumerable collection of WpfScreen instances representing all screens.</returns>
        public static IEnumerable<WpfScreen> AllScreens()
        {
            foreach (System.Windows.Forms.Screen screen in System.Windows.Forms.Screen.AllScreens)
            {
                yield return new WpfScreen(screen);
            }
        }

        /// <summary>
        /// Retrieves the screen that contains the specified window.
        /// </summary>
        /// <param name="window">The window for which to retrieve the screen.</param>
        /// <returns>The WpfScreen instance representing the screen containing the window.</returns>
        public static WpfScreen GetScreenFrom(Window window)
        {
            WindowInteropHelper windowInteropHelper = new WindowInteropHelper(window);
            System.Windows.Forms.Screen screen = System.Windows.Forms.Screen.FromHandle(windowInteropHelper.Handle);
            WpfScreen wpfScreen = new WpfScreen(screen);
            return wpfScreen;
        }

        /// <summary>
        /// Retrieves the screen that contains the specified point.
        /// </summary>
        /// <param name="point">The point on the screen for which to retrieve the screen.</param>
        /// <returns>The WpfScreen instance representing the screen containing the point.</returns>
        public static WpfScreen GetScreenFrom(Point point)
        {
            int x = (int)Math.Round(point.X);
            int y = (int)Math.Round(point.Y);

            // are x,y device-independent-pixels ??
            System.Drawing.Point drawingPoint = new System.Drawing.Point(x, y);
            WinForms.Screen screen = System.Windows.Forms.Screen.FromPoint(drawingPoint);
            WpfScreen wpfScreen = new WpfScreen(screen);

            return wpfScreen;
        }

        /// <summary>
        /// Gets the primary screen.
        /// </summary>
        public static WpfScreen Primary
        {
            get { return new WpfScreen(System.Windows.Forms.Screen.PrimaryScreen); }
        }

        /// <summary>
        /// Gets the bounds of the screen in device-independent units.
        /// </summary>
        public Rect DeviceBounds
        {
            get { return this.GetRect(this.screen.Bounds); }
        }

        /// <summary>
        /// Gets the working area of the screen in device-independent units.
        /// </summary>
        public Rect WorkingArea
        {
            get { return this.GetRect(this.screen.WorkingArea); }
        }

        private Rect GetRect(System.Drawing.Rectangle value)
        {
            // should x, y, width, height be device-independent-pixels ??
            return new Rect
            {
                X = value.X,
                Y = value.Y,
                Width = value.Width,
                Height = value.Height
            };
        }

        /// <summary>
        /// Gets a value indicating whether the screen is the primary screen.
        /// </summary>
        public bool IsPrimary
        {
            get { return this.screen.Primary; }
        }

        /// <summary>
        /// Gets the device name associated with the screen.
        /// </summary>
        public string DeviceName
        {
            get { return this.screen.DeviceName; }
        }
    }
}
