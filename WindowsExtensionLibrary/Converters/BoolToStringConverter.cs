﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace WindowsExtensionLibrary.Converters
{
    [ValueConversion(typeof(bool), typeof(string))]
    public class BoolToStringConverter : IValueConverter
    {
        public string TrueString { get; set; }
        public string FalseString { get; set; }
        public string NullString { get; set; } = string.Empty;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return NullString;

            if (value is bool isValueTrue)
            {
                return isValueTrue ? TrueString : FalseString;
            }

            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
