﻿using System.Windows;

namespace WindowsExtensionLibrary.Helpers
{
    /// <summary>
    /// Attached property for button invoke Snap layout.
    /// </summary>
    public class SnapButtonHelper
    {
        public static readonly DependencyProperty IsMouseOverButtonWidthSnapProperty =
            DependencyProperty.RegisterAttached("IsMouseOverButtonWidthSnap", typeof(bool), typeof(SnapButtonHelper), new PropertyMetadata(default(bool)));

        public static void SetIsMouseOverButtonWidthSnap(DependencyObject element, bool value)
        {
            element.SetValue(IsMouseOverButtonWidthSnapProperty, value);
        }

        public static bool GetIsMouseOverButtonWidthSnap(DependencyObject element)
        {
            return (bool)element.GetValue(IsMouseOverButtonWidthSnapProperty);
        }

        public static readonly DependencyProperty IsPressedButtonWidthSnapProperty =
            DependencyProperty.RegisterAttached("IsPressedButtonWidthSnap", typeof(bool), typeof(SnapButtonHelper), new PropertyMetadata());

        public static void SetIsPressedButtonWidthSnap(DependencyObject element, bool value)
        {
            element.SetValue(IsPressedButtonWidthSnapProperty, value);
        }

        public static bool GetIsPressedButtonWidthSnap(DependencyObject element)
        {
            return (bool)element.GetValue(IsPressedButtonWidthSnapProperty);
        }

    }
}
